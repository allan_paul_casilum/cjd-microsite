(function() {
	tinymce.create('tinymce.plugins.CJDshortcode', {
          init : function(editor, url) {
					var menu_button_label = 'Techriver - Microsite';
					var list_menu = [
						{
							text:'APP',
							menu:[
								cjd_add_microsite(editor)
							]
						}
					];

					editor.addButton('cjdshortcodes', {
						type: 'menubutton',
						icon: false,
						text: menu_button_label,
						menu: list_menu
					});
          },
          createControl : function(n, cm) {
               return null;
          }
     });
	/* Start the buttons */
    tinymce.PluginManager.add('cjdshortcodes', tinymce.plugins.CJDshortcode );
})();

