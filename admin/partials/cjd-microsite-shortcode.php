<?php
if( $meta ){
	$micro_meta = $meta[0];
	$default_image = get_stylesheet_directory_uri().'/images/latest-news-img.jpg';
	
	?>
	
		</div>
		<div class="row">
			<div class="banner-micro-sty">
				<figure>
					<?php if( isset($micro_meta['microsite_banner_img']) ){ ?>
					<img src="<?php echo $micro_meta['microsite_banner_img']; ?>" alt="banner-img"> 
					<?php } ?>
				</figure>

				<div class="clearfix"></div>
				
				<div class="banner-muslims-sty hidden">
					<ul class="list-inline">
						<li> <a href="#MUSLIM_BROTHERHOOD"> WHAT IS THE MUSLIM BROTHERHOOD? </a> </li>
						<li> <a href="#THE_BROTHERHOOD"> THE BROTHERHOOD ON JIHAD  </a></li>
						<li> <a href="#PARTICIPATION_IN_TERRORISM"> PARTICIPATION IN TERRORISM </a></li>
						<li> <button> <a href="#take_action">Take Action</a></button> </li>
					</ul>
				</div>

				<div class="clearfix"></div>
			</div>
		</div>
		<?php if( isset($micro_meta['repeating_textareas']) ){ ?>
			<?php foreach( $micro_meta['repeating_textareas'] as $key => $val ){ ?>
				<div class="section-cls-sty"> 
					<div class="micro-spacing section-container" style="background-image: url(<?php echo $val['imgurl'] ?>);">
						<div class="section-title">
							<h1><?php echo $val['title'];?></h1>
						</div>
						<div class="section-content">
							<?php echo $val['textarea']; ?>
						</div>
					</div>
				</div>
			<?php }//foreach repeating textarea ?>
		<?php }//isset repeating textarea ?>
		
		<!-- Timeline start--> 
			<div class="section-cls-sty promotion-cls-div">
			  <section id="cd-timeline" class="cd-container block">
				<?php 
				$leftSide = true;
				if( isset($micro_meta['repeating_timeline']) ){
					foreach( $micro_meta['repeating_timeline'] as $key => $val ){
						if ($leftSide)
							$timelineStyle = "cd-left-content";
						else
							$timelineStyle = "cd-right-content";
						$leftSide = !$leftSide;
				?>
					
				<div class="cd-timeline-block">
				  <div class="cd-timeline-img cd-picture  wow zoomIn animated" >
					  <span><?php echo $val['year'];?></span>
				  </div> <!-- cd-timeline-img -->
				  <div class="cd-timeline-content <?php echo $timelineStyle ?> wow fadeInLeft animated">
					  <h2><?php echo $val['title']; ?></h2>
					  <p><?php echo $val['description']; ?></p>
				  </div> <!-- cd-timeline-content -->
				</div> <!-- cd-timeline-block -->
				<?php
					}//foreach
				}//if
				?>
			  </section>
			</div>
		<!-- Timeline ends--> 
		
	<?php /*if meta */
}
?>
