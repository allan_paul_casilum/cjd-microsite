<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              apysais.com
 * @since             1.0.0
 * @package           Cjd_Microsite
 *
 * @wordpress-plugin
 * Plugin Name:       cjd microsite
 * Plugin URI:        techriver.net
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Allan Casilum
 * Author URI:        apysais.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       cjd-microsite
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
spl_autoload_register('cjd_autoload_class');
function cjd_autoload_class($class_name){
    if ( false !== strpos( $class_name, 'CJD' ) ) {
		$include_classes_dir = realpath( plugin_dir_path( __FILE__ ) ) . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR;
		$admin_classes_dir = realpath( plugin_dir_path( __FILE__ ) ) . DIRECTORY_SEPARATOR . 'admin' . DIRECTORY_SEPARATOR;
		$class_file = str_replace( '_', DIRECTORY_SEPARATOR, $class_name ) . '.php';
		if( file_exists($include_classes_dir . $class_file) ){
			require_once $include_classes_dir . $class_file;
		}
		if( file_exists($admin_classes_dir . $class_file) ){
			require_once $admin_classes_dir . $class_file;
		}
	}
}
/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-cjd-microsite-activator.php
 */
function activate_cjd_microsite() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-cjd-microsite-activator.php';
	Cjd_Microsite_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-cjd-microsite-deactivator.php
 */
function deactivate_cjd_microsite() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-cjd-microsite-deactivator.php';
	Cjd_Microsite_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_cjd_microsite' );
register_deactivation_hook( __FILE__, 'deactivate_cjd_microsite' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-cjd-microsite.php';

add_action( 'plugins_loaded', 'run_cjd_microsite' ); // Hook initialization function
/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_cjd_microsite() {
	global $config, $wpalchemy_media_access;
	
	require_once(ABSPATH.'wp-admin/includes/plugin.php');
	$plugin_data = get_plugin_data( __FILE__ );
	
	$config = array();
	$config['dir'] = plugin_dir_path( __FILE__ );
	$config['url'] = plugins_url() . '/' . $plugin_data['TextDomain'] . '/';
	$config['admin_partials'] = $config['dir'] . 'admin/partials/';
	
	require plugin_dir_path( __FILE__ ) . 'includes/functions.php';
	
	$plugin = new Cjd_Microsite();
	
	//load metabox class, alchemy
	$alchemy = new CJD_Alchemy;
	$alchemy->init();
	
	$wpalchemy_media_access = new WPAlchemy_MediaAccess;
	
	$metabox = new CJD_Metabox;
	$metabox->init();
	//load metabox class, alchemy
	
	//hooks
	$plugin->get_loader()->add_action( 'init', CJD_CPTMicrosite::get_instance(), 'cpt_init' );
	//$plugin->get_loader()->add_action( 'save_post', CJD_CPTMicrosite::get_instance(), 'my_project_updated_send_email' );
	$plugin->get_loader()->add_action( 'admin_print_scripts-post-new.php', CJD_CPTMicrosite::get_instance(), 'tabs_scripts', 11 );
	$plugin->get_loader()->add_action( 'admin_print_scripts-post.php', CJD_CPTMicrosite::get_instance(), 'tabs_scripts', 11 );
	$plugin->get_loader()->add_action( 'admin_enqueue_scripts', CJD_CPTMicrosite::get_instance(), 'my_admin_enqueue_scripts' );
	
	/* 
	 * Recreate the default filters on the_content
	 * this will make it much easier to output the meta content with proper/expected formatting
	*/
	add_filter( 'meta_content', 'wptexturize'        );
	add_filter( 'meta_content', 'convert_smilies'    );
	add_filter( 'meta_content', 'convert_chars'      );
	add_filter( 'meta_content', 'wpautop'            );
	add_filter( 'meta_content', 'shortcode_unautop'  );
	add_filter( 'meta_content', 'prepend_attachment' );
	add_filter( 'meta_content', 'do_shortcode');
	
	CJD_Shortcode::get_instance();
	
	$plugin->run();
}


