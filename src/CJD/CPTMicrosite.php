<?php
class CJD_CPTMicrosite{
	protected static $instance = null;
	
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
	
	public function cpt_init(){
		$labels = array(
			'name'               => _x( 'Microsite', 'post type general name', 'your-plugin-textdomain' ),
			'singular_name'      => _x( 'Microsite', 'post type singular name', 'your-plugin-textdomain' ),
			'menu_name'          => _x( 'Microsite', 'admin menu', 'your-plugin-textdomain' ),
			'name_admin_bar'     => _x( 'Microsite', 'add new on admin bar', 'your-plugin-textdomain' ),
			'add_new'            => _x( 'Add New', 'microsite', 'your-plugin-textdomain' ),
			'add_new_item'       => __( 'Add New Microsite', 'your-plugin-textdomain' ),
			'new_item'           => __( 'New Microsite', 'your-plugin-textdomain' ),
			'edit_item'          => __( 'Edit Microsite', 'your-plugin-textdomain' ),
			'view_item'          => __( 'View Microsite', 'your-plugin-textdomain' ),
			'all_items'          => __( 'All Microsites', 'your-plugin-textdomain' ),
			'search_items'       => __( 'Search Microsites', 'your-plugin-textdomain' ),
			'parent_item_colon'  => __( 'Parent Microsites:', 'your-plugin-textdomain' ),
			'not_found'          => __( 'No Microsites found.', 'your-plugin-textdomain' ),
			'not_found_in_trash' => __( 'No Microsites found in Trash.', 'your-plugin-textdomain' )
		);

		$args = array(
			'labels'             => $labels,
			'description'        => __( 'Description.', 'your-plugin-textdomain' ),
			'public'             => false,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'microsite' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'author', 'custom-fields', 'page-attributes')
		);

		register_post_type( 'microsite', $args );
	}
	
	
	public function tabs_scripts() {
		global $post_type;
		if( $post_type == 'microsite' )
		  wp_enqueue_script('tabs-admin-script', home_url() . '/wp-includes/js/tinymce/tinymce.min.js');
		  wp_enqueue_script('tabs-admin-editor', home_url() . '/wp-admin/js/editor.js');
		  wp_enqueue_style('tabs-tinymce-styles', home_url() . '/wp-includes/js/tinymce/skins/lightgray/skin.min.css' );
		  wp_enqueue_style('tabs-editor-styles', home_url() . '/wp-includes/css/editor.min.css' );
	}
	
	public function my_admin_enqueue_scripts() {
		if ( 'microsite' == get_post_type() )
			wp_dequeue_script( 'autosave' );
	}
	
	public function my_project_updated_send_email($post_id){
		echo '<pre>';
		print_r($_POST);
		echo '</pre>';
		exit();
	}
	
}
