<?php
class CJD_Alchemy{
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
	
	private function include_alchemy(){
		require_once cjd_config_dir() . 'includes/wpalchemy/MetaBox.php';
		require_once cjd_config_dir() . 'includes/wpalchemy/MediaAccess.php';
	}
	
	public function init(){
		$this->include_alchemy();
		// global styles for the meta boxes
		if (is_admin()) add_action('admin_enqueue_scripts', array($this,'metabox_style') );
	}
	
	public function metabox_style() {
		$path = cjd_config_url() . 'includes/wpalchemy/metaboxes/meta.css';
		wp_enqueue_style('wpalchemy-metabox', $path);
	}
	
	public function __construct(){}
	
}
