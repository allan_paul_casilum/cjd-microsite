<?php
class CJD_Shortcode{
	protected static $instance = null;
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
	
	public function get_list(){
		$list_array = array();
		$args = array(
			'post_type' => 'microsite',
			'posts_per_page' => -1, 
			'post_status' =>'publish'
		);
		$posts_array = get_posts( $args );

		foreach($posts_array as $key => $val){
			$list_array[$val->ID] = $val->post_title;
		}
		
		return $list_array;
	}
	
	/**
	 * Add shortcode JS to the page
	 *
	 * @return HTML
	 */
	public function js_shortcodes(){
		?>
			<script type="text/javascript">
				function cjd_add_microsite(editor){
					var mc_list = [
						<?php if( count($this->get_list()) > 0 ){ ?>
								<?php foreach($this->get_list() as $key=>$val){ ?>
										{text: '<?php echo $val; ?>',value: '<?php echo $key;?>'},
								<?php } ?>
						<?php } ?>
					];
					var submenu_array =
					{
						text: 'Add Microsite Content',
						onclick: function() {
							editor.windowManager.open( {
								title: 'Choose Microsite Content',
								width:980,
								height:350,
								body: [
									{
										type: 'listbox',
										name: 'listboxMicrositeList',
										label: 'Choose Microsite Content',
										'values': mc_list
									},
								],
								onsubmit: function( e ) {
									var microsite_post = ' cjd_microsite_id="' + e.data.listboxMicrositeList + '" ';
									editor.insertContent(
										'[cjd_microsite_content ' + microsite_post + ']'
									);
								}
							});
						}
					};
					return submenu_array;
				}
			</script>
		<?php
	}
	
	public function init_shortcode($atts){
		global $cjd_metabox;

		$microsite_id = '';
		if( isset($atts['cjd_microsite_id']) ){
			$microsite_id = $atts['cjd_microsite_id'];
		}
		$a = shortcode_atts( array(
			'cjd_microsite_id' => $microsite_id,
		), $atts );
		$post_id = $microsite_id;
		$meta = get_post_meta($post_id, $cjd_metabox->get_the_id());

		ob_start();
		require_once cjd_config_admin_partials() . 'cjd-microsite-shortcode.php';
		$output = ob_get_clean();
		return $output;
	}
	
	public function __construct(){
		add_action('admin_footer', array($this, 'js_shortcodes'));
		add_shortcode('cjd_microsite_content', array($this,'init_shortcode'));
	}
}
