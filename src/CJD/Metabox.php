<?php
class CJD_Metabox{
	protected static $instance = null;
	public $cjd_metabox;
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
	
	public function get_cjd_metabox(){
		return $this->cjd_metabox;
	}
	
	private function load_metaboxes(){
		global $cjd_metabox;
		
		$cjd_metabox = new WPAlchemy_MetaBox(array
		(
			'id' => '_cjd_metabox',
			'title' => 'Microsite',
			'types' => array('microsite'), // added only for pages and to custom post type "events"
			'context' => 'normal', // same as above, defaults to "normal"
			'priority' => 'high', // same as above, defaults to "high"
			'prefix' => '_cjd_',
			'template' => cjd_config_dir() . 'includes/wpalchemy/metaboxes/cjd-meta.php'
		));
	}
	
	public function init(){
		$this->load_metaboxes();
	}
	
}
