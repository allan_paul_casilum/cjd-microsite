<?php

/**
 * Fired during plugin activation
 *
 * @link       apysais.com
 * @since      1.0.0
 *
 * @package    Cjd_Microsite
 * @subpackage Cjd_Microsite/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Cjd_Microsite
 * @subpackage Cjd_Microsite/includes
 * @author     Allan Casilum <allan.paul.casilum@gmail.com>
 */
class Cjd_Microsite_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
