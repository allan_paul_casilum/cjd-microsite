<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
function cjd_config($key = null){
	global $config;
	
	if( !is_null($key) 
		&& isset($config[$key])
	){
		return $config[$key];
	}
	return $config;
}
function cjd_config_dir(){
	return cjd_config('dir');
}
function cjd_config_url(){
	return cjd_config('url');
}
function cjd_config_admin_partials(){
	return cjd_config('admin_partials');
}
