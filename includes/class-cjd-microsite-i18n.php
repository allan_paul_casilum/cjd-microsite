<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       apysais.com
 * @since      1.0.0
 *
 * @package    Cjd_Microsite
 * @subpackage Cjd_Microsite/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Cjd_Microsite
 * @subpackage Cjd_Microsite/includes
 * @author     Allan Casilum <allan.paul.casilum@gmail.com>
 */
class Cjd_Microsite_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'cjd-microsite',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
