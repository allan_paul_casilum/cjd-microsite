<?php

/**
 * Fired during plugin deactivation
 *
 * @link       apysais.com
 * @since      1.0.0
 *
 * @package    Cjd_Microsite
 * @subpackage Cjd_Microsite/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Cjd_Microsite
 * @subpackage Cjd_Microsite/includes
 * @author     Allan Casilum <allan.paul.casilum@gmail.com>
 */
class Cjd_Microsite_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
