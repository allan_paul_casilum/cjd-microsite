<?php global $wpalchemy_media_access; ?>
<div class="my_meta_control">
	
	<h4>Banner Image</h4>
	<div class="banner-image">
		<p>Add image for Banner</p>
			
		<div class="banner-img-metabox">
			<?php $mb->the_field('microsite_banner_img'); ?>
			<p>
				<input id="microsite_banner_img" type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/>
				<input id="bannerimg-button" type="button" class="button" value="Upload / Choose Banner Image" />
			</p>
		</div>

	</div>
	<hr>
	<h3>Section Content</h3>
	<?php while( $mb->have_fields_and_multi( 'repeating_textareas' ) ): ?>
	<?php $mb->the_group_open(); ?>
		<h3 class="section-content-group-title">Toggle Content</h3>

		<div class="section-content-group-wrap ">
			<p>
				<span>Add Title</span>
				<input type="text" name="<?php $mb->the_name('title'); ?>" value="<?php $mb->the_value('title'); ?>"/>
			</p>
			<div class="section-content-group-wrap-content">
				<?php $mb->the_field('imgurl'); ?>
				<?php $wpalchemy_media_access->setGroupName('img-n'. $mb->get_the_index())->setInsertButtonLabel('Insert'); ?>
				<p>
					<span>Add Background Image</span>
					<?php echo $wpalchemy_media_access->getField(array('name' => $mb->get_the_name(), 'value' => $mb->get_the_value())); ?>
					<?php echo $wpalchemy_media_access->getButton(); ?>
				</p>
				
				<?php $mb->the_field('textarea'); ?>
			
				<div class="group-inside">
					<?php
					// 'html' is used for the "Text" editor tab.
					if ( 'html' === wp_default_editor() ) {
						add_filter( 'the_editor_content', 'format_for_editor' );
						$switch_class = 'html-active';
					} else {
						add_filter( 'the_editor_content', 'format_for_editor' );
						$switch_class = 'tmce-active';
					}
					?>

					<div class="customEditor wp-core-ui wp-editor-wrap <?php echo 'tmce-active'; echo $switch_class;?>">
						
						<div class="wp-editor-tools hide-if-no-js">

							<div class="wp-media-buttons custom_upload_buttons">
								<?php do_action( 'media_buttons' ); ?>
							</div>

							<div class="wp-editor-tabs">
								<a data-mode="html" class="wp-switch-editor switch-html"> <?php _ex( 'Text', 'Name for the Text editor tab (formerly HTML)' ); ?></a>
								<a data-mode="tmce" class="wp-switch-editor switch-tmce"><?php _e('Visual'); ?></a>
							</div>

						</div><!-- .wp-editor-tools -->

						<div class="wp-editor-container">
							<textarea class="wp-editor-area" rows="10" cols="50" name="<?php $mb->the_name(); ?>" rows="3"><?php echo esc_html( apply_filters( 'the_editor_content', html_entity_decode( $mb->get_the_value() ) ) ); ?></textarea>
						</div>
						<p><a href="#" class="dodelete button">Remove Section</a></p>
					</div>
				</div><!-- .group-inside -->
			</div><!-- .section-content-group-wrap-content -->
		</div><!-- .section-content-wrap -->

	<?php $mb->the_group_close(); ?>
	<?php endwhile; ?>

	<p><a href="#" class="docopy-repeating_textareas button"><span class="icon add"></span>Add Section</a></p>	
	<p class="meta-save"><button type="submit" class="button-primary" name="save"><?php _e('Update');?></button></p>
	<hr>
	
	<h4>Timeline</h4>
	
	<?php while( $mb->have_fields_and_multi( 'repeating_timeline' ) ): ?>
	<?php $mb->the_group_open(); ?>
		<div class="timeline-content-wrap">
			<p>
				<span>Enter in Year</span>
				<?php $mb->the_field('year'); ?>
				<input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/>
			</p>
			<p>
				<span>Enter in Title</span>
				<?php $mb->the_field('title'); ?>
				<input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/>
			</p>
			<p>
				<span>Enter in Description</span>
				<?php $mb->the_field('description'); ?>
				<textarea name="<?php $mb->the_name(); ?>" rows="10" cols="50"><?php $mb->the_value(); ?></textarea>
			</p>
			<p><a href="#" class="dodelete button">Remove Timeline</a></p>
		</div>
	<?php $mb->the_group_close(); ?>
	<?php endwhile; ?>
	<p><a href="#" class="docopy-repeating_timeline button"><span class="icon add"></span>Add Timeline</a></p>	
	<p class="meta-save"><button type="submit" class="button-primary" name="save"><?php _e('Update');?></button></p>
</div>
<script>
jQuery(document).ready(function($){

  var mediaUploader;
  
  function init_media_uploader(){
	if (mediaUploader) {
		mediaUploader.open();
		return;
	}
	// Extend the wp.media object
	mediaUploader = wp.media.frames.file_frame = wp.media({
		title: 'Choose Image',
		button: {
		text: 'Choose Image'
	}, multiple: false });

	// Open the uploader dialog
	mediaUploader.open();
  }
  	
  $('#bannerimg-button').click(function(e) {
    e.preventDefault();
    mediaUploader = null;
    // If the uploader object has already been created, reopen the dialog
    init_media_uploader();
   
	// When a file is selected, grab the URL and set it as the text field's value
	mediaUploader.on('select', function() {
		attachment = mediaUploader.state().get('selection').first().toJSON();
		//$('.mwp-logo-preview').attr('src',attachment.url);
		$('#microsite_banner_img').val(attachment.url);
	});
	
  });
  $(document).on('click','[class*=mediabutton-img]', function(e){
	mediaUploader = null;
	e.preventDefault();
	var curr_class = $(this).attr('class');
	var get_indx = curr_class.match(/\d+/); // 123456
	// If the uploader object has already been created, reopen the dialog
	init_media_uploader();

	// When a file is selected, grab the URL and set it as the text field's value
	mediaUploader.on('select', function() {
		attachment = mediaUploader.state().get('selection').first().toJSON();
		$('.mediafield-img-n' + get_indx[0]).val(attachment.url);
	});
  });
  $('#wpa_loop-repeating_textareas')
	.sortable({
		//cancel: ':input,button,.customEditor', // exclude TinyMCE area from the sort handle
		axis: 'y',
		tolerance: 'pointer',
		placeholder: 'ui-state-highlight',
		start: function(event, ui) { // turn TinyMCE off while sorting (if not, it won't work when resorted)
			textareaID = $(ui.item).find('textarea.wp-editor-area').attr('id');
			try { tinyMCE.execCommand('mceRemoveEditor', false, textareaID); } catch(e){}
		},
		stop: function(event, ui) { // re-initialize TinyMCE when sort is completed
			try { tinyMCE.execCommand('mceAddEditor', false, textareaID); } catch(e){}
		}
	});

  $('#wpa_loop-repeating_timeline').sortable({
		//cancel: ':input,button,.customEditor', // exclude TinyMCE area from the sort handle
		axis: 'y',
		tolerance: 'pointer',
		placeholder: 'ui-state-highlight',
		start: function(event, ui) { // turn TinyMCE off while sorting (if not, it won't work when resorted)
			textareaID = $(ui.item).find('textarea.wp-editor-area').attr('id');
			try { tinyMCE.execCommand('mceRemoveEditor', false, textareaID); } catch(e){}
		},
		stop: function(event, ui) { // re-initialize TinyMCE when sort is completed
			try { tinyMCE.execCommand('mceAddEditor', false, textareaID); } catch(e){}
		}
	});
	
	$(document).on('click', '.wpa_loop-repeating_textareas .section-content-group-title', function(e){
		var $group = $(this).parents('.wpa_group-repeating_textareas');
		var $inside = $group.find('.section-content-group-wrap-content');
		$inside.toggle('slow');
	});

});
</script>
