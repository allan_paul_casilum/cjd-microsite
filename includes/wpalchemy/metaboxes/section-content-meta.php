<div class="my_meta_control">
	<?php global $cjd_metabox, $wpalchemy_media_access; ?>
	<h4>Banner Image</h4>
	<div class="banner-image">
		<p>Add image for Banner</p>
			
		<div class="banner-img-metabox">
			<?php $cjd_metabox->the_field('microsite_banner_img'); ?>
			<?php $wpalchemy_media_access->setGroupName('nn')->setInsertButtonLabel('Insert This')->setTab('gallery'); ?>
			<p>
			<?php echo $wpalchemy_media_access->getField(array('name' => $cjd_metabox->get_the_name(), 'value' => $cjd_metabox->get_the_value())); ?>
			<?php echo $wpalchemy_media_access->getButton(array('label' => 'Add Image From Library')); ?>
			</p>
		</div>

	</div>
	
	<hr>
	
	<h4>Section</h4>
 
	<a style="float:right; margin:0 10px;" href="#" class="dodelete-sectioncontent button">Remove All</a>
 
	<p>Add documents to the library by entering in a title, 
	URL and selecting a level of access. Upload new documents 
	using the "Add Media" box.</p>

	<?php while($cjd_metabox->have_fields_and_multi('sectioncontent')): ?>
	<?php $cjd_metabox->the_group_open(); ?>
		<div class="cjd-content">
			<?php $cjd_metabox->the_field('title'); ?>
			<label>Title and URL</label>
			<p><input type="text" name="<?php $cjd_metabox->the_name(); ?>" value="<?php $cjd_metabox->the_value(); ?>"/></p>
			
			 <?php $cjd_metabox->the_field('content'); ?>
			 <?php
				$mb_content = html_entity_decode($cjd_metabox->get_the_value(), ENT_QUOTES, 'UTF-8');
				$mb_editor_id = sanitize_key($cjd_metabox->get_the_name());
				$mb_settings = array('textarea_name'=>$cjd_metabox->get_the_name(),'textarea_rows' => '5',);
				wp_editor( $mb_content, $mb_editor_id, $mb_settings );
			?>
			<?php 
			/*wp_editor( '', sanitize_key($cjd_metabox->get_the_name()), array(
				'wpautop'       => true,
				'textarea_name' => 'repeatable_editor_repeatable_editor_content[]',
				'textarea_rows' => 10,
			) ); */
			?>
			<p><a href="#" class="dodelete button">Remove Document</a></p>
		</div>
	<?php $cjd_metabox->the_group_close(); ?>
	<?php endwhile; ?>
 
	<p style="margin-bottom:15px; padding-top:5px;"><a href="#" class="docopy-sectioncontent button">Add Document</a></p>

</div>
